Source: pipe-viewer
Section: perl
Priority: optional
Maintainer: Antoni Aloy Torrens <aaloytorrens@gmail.com>
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl,
               libjson-perl,
               libjson-xs-perl,
               libfile-sharedir-perl,
               libmodule-build-perl,
               gir1.2-gtk-3.0,         
               libgtk3-perl
Standards-Version: 3.9.8
Homepage: https://metacpan.org/release/Filter/
Vcs-Git: https://salsa.debian.org/debian/libfilter-perl.git
Vcs-Browser: https://salsa.debian.org/debian/libfilter-perl
Testsuite: autopkgtest-pkg-perl
Rules-Requires-Root: no

Package: pipe-viewer
Architecture: any
Depends: ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends},
         yt-dlp | youtube-dl,
         libjson-perl,
         libjson-xs-perl,
         libfile-sharedir-perl,
Recommends: mpv,
            ffmpeg,
            liblwp-useragent-chicaching-perl,
            libterm-readline-gnu-perl,
Suggests: vlc,
          wget,
Description: Search and play videos from YouTube
 A lightweight application (fork of straw-viewer)
 for searching and playing videos from YouTube.
 .
 This fork parses the YouTube website directly and
 relies on the invidious instances only as a fallback
 method. Source filters alter the program text of a
 module before Perl sees it, much as a C preprocessor
 alters the source text of a C program before the
 compiler sees it.

Package: gtk-pipe-viewer
Architecture: any
Depends: ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends},
         pipe-viewer,
         gir1.2-gtk-3.0,         
         libgtk3-perl,
Recommends: ${pipe-viewer:Recommends},
            gnome-icon-theme,
            mpv,
            gnome-mpv,
Suggests: ${pipe-viewer:Suggests},
Description: Search and play videos from YouTube
 A lightweight application (fork of straw-viewer)
 for searching and playing videos from YouTube (GTK).
 .
 This fork parses the YouTube website directly and
 relies on the invidious instances only as a fallback
 method. Source filters alter the program text of a
 module before Perl sees it, much as a C preprocessor
 alters the source text of a C program before the
 compiler sees it.
